
public class TestNode1 {
	public static void main (String args[]) {
		Node emptyList = null; //membuat list kosong
		
		//head points untuk node pertama dalam list
		Node head = new Node();
		
		//inisialisasi node pertama dalam list
		head.data = "Fitri Nuraida";
		head.nextNode = new Node();
		head.nextNode.data = "3411181041";
		
		//null menandai akhir dari list
		head.nextNode= null;
		
		//mencetak elemen list
		Node currNode = head;
		while (currNode != null) {
			System.out.println(currNode.data);
			currNode = currNode.nextNode;
		}
	}
}
