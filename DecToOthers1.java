import java.util.Scanner;

public class DecToOthers1 {

	public static void main(String args[]) {
		int num;
		int base;
		Scanner sc = new Scanner(System.in);
		System.out.println("Masukkan num : ");
		num = sc.nextInt();
		System.out.println("Masukkan base : ");
		base = sc.nextInt();
		printBase(num, base);
	}
	
	static void printBase (int num, int base) {
		int rem =1;
		String digits = "0123456789abcdef";
		String result = "";
		
		/* Langkah Iterasi */
		while (num != 0) {
			rem = num%base;
			num = num/base;
			result = result.concat(digits.charAt(rem)+"");
		}
		
		/*mencetak reverse dari result */
		for (int i = result.length()-1; i >= 0; i--) {
			System.out.print(result.charAt(i));
		}
	}
}
