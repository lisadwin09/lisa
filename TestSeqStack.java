public class TestSeqStack {
	public static void main(String args[]) {
		SeqStack stack = new SeqStack();
		stack.push(1);
		stack.push(8);
		stack.push(5);
		stack.push(7);
		stack.push(14);
		System.out.print("Isi Stack : ");
		for (int i=stack.top; i>=0; i--){
			System.out.print(stack.memSpace[i] + " ");
		}
		System.out.println("");
		System.out.println("Top : " +stack.memSpace[stack.top]);
		System.out.println("Push : " +stack.pop());
		System.out.print("Isi Stack : ");
		for (int i=stack.top; i>=0; i--){
			System.out.print(stack.memSpace[i] + " ");
		}
		System.out.println("");
		System.out.println("Top : " +stack.memSpace[stack.top]);
		System.out.println("Push : " +stack.pop());
		System.out.print("Isi Stack : ");
		for (int i=stack.top; i>=0; i--){
			System.out.print(stack.memSpace[i] + " ");
		}
	}
}
